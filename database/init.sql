
CREATE DATABASE IF NOT EXISTS demo_vault_dev;
USE demo_vault_dev;

CREATE TABLE IF NOT EXISTS `users` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    age INT
);

INSERT INTO `users` (full_name, age) VALUES ('Cristiano Ronaldo DEV', 38);
INSERT INTO `users` (full_name, age) VALUES ('Lionel Messi DEV', 36);



CREATE DATABASE IF NOT EXISTS demo_vault_uat;
USE demo_vault_uat;

CREATE TABLE IF NOT EXISTS `users` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    age INT
);

INSERT INTO `users` (full_name, age) VALUES ('Cristiano Ronaldo UAT', 38);
INSERT INTO `users` (full_name, age) VALUES ('Lionel Messi UAT', 36);



CREATE DATABASE IF NOT EXISTS demo_vault_prod;
USE demo_vault_prod;

CREATE TABLE IF NOT EXISTS `users` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    age INT
);

INSERT INTO `users` (full_name, age) VALUES ('Cristiano Ronaldo PROD', 38);
INSERT INTO `users` (full_name, age) VALUES ('Lionel Messi PROD', 36);