#!/bin/bash
bash remove.sh
VAULT_TOKEN=$VAULT_TOKEN

if [ -z "$VAULT_TOKEN" ]
then
    echo "Please provide your Vault Token to continue. If you don't have one, please contact your admin for more details"
    exit 0
fi

echo $VAULT_TOKEN

VAULT_SECRET_PATH="v1/envs/data"

display_env_menu() {
    echo "Enter environment:"
    echo "1. DEV"
    echo "2. UAT"
    echo "3. PROD"
    echo "4. Quit"
}

select_env() {
    read -rp "Enter your choice: " choice
    case $choice in
        1)
            echo "You have set your environment to DEV"
            VAULT_SECRET_PATH="$VAULT_SECRET_PATH/dev"
            ;;
        2)
            echo "You have set your environment to UAT"
            VAULT_SECRET_PATH="$VAULT_SECRET_PATH/uat"
            ;;
        3)
            echo "You have set your environment to PROD"
            VAULT_SECRET_PATH="$VAULT_SECRET_PATH/prod"
            ;;
        4)
            echo "Exiting wizard..."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter a valid option."
            handle_selection
            ;;
    esac
}

check=true
while $check; do
    display_env_menu
    select_env
    check=false
done

display_region_menu() {
    echo "Enter region:"
    echo "1. Australia (AU)"
    echo "2. USA (US)"
    echo "3. Quit"
}

select_region() {
    read -rp "Enter your choice: " choice
    case $choice in
        1)
            echo "You have set your region to AU"
            VAULT_SECRET_PATH="$VAULT_SECRET_PATH/au"
            ;;
        2)
            echo "You have set your region to US"
            VAULT_SECRET_PATH="$VAULT_SECRET_PATH/us"
            ;;
        3)
            echo "Exiting wizard..."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter a valid option."
            handle_selection
            ;;
    esac
}

check=true
while $check; do
    display_region_menu
    select_region
    check=false
done

json_data=$(curl --header "X-Vault-Token: $VAULT_TOKEN" --request GET $VAULT_ADDR/$VAULT_SECRET_PATH)

if [[ $json_data == *"error"* ]]; then
  echo "You don't have permissions to access this data, please contact your admin for more details"
  exit 0
fi

json_data=$(echo "$json_data" | jq -r '.data.data')

ENV_VARS=$(echo "$json_data" | jq -r 'to_entries|map("-e \(.key)=\(.value|tostring)")|.[]')

cd ./app
mvn clean package -DskipTests
cd ..

docker build -t vault-demo ./app
docker build -t vault-mysql ./database

docker network create app-network

docker run -d -p 3306:3306 \
    --network app-network \
    $ENV_VARS \
    --name vault-mysql-container vault-mysql

sleep 5

docker run -d -p 8080:8080 \
    --network app-network \
    $ENV_VARS \
    --name vault-demo-container vault-demo
