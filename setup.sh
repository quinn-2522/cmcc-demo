#!/bin/bash

# Install required dependencies
sudo yum update -y
sudo yum install -y unzip

# Download and install Vault
VAULT_VERSION="1.9.0"  # Update with the latest version
wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip
unzip vault_${VAULT_VERSION}_linux_amd64.zip
sudo mv vault /usr/local/bin/
rm vault_${VAULT_VERSION}_linux_amd64.zip

# Create a Vault configuration file
sudo mkdir -p /etc/vault.d
sudo tee /etc/vault.d/vault.hcl <<EOF
storage "file" {
  path = "/opt/vault/data"
}

listener "tcp" {
  address       = "0.0.0.0:8200"
  tls_disable   = 1
}

api_addr = "http://127.0.0.1:8200"
cluster_addr = "https://127.0.0.1:8201"
ui = true
EOF

export VAULT_ADDR=http://127.0.0.1:8200
sudo /usr/local/bin/vault server -config=/etc/vault.d/vault.hcl 

# vault operator init > init.txt